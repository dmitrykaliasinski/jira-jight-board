import { useForm } from 'react-hook-form';
import { Button } from '@mui/material';

import './createSectionInput.css';

const CreateSectionInput = ({ createSection }) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {
      sectionName: '',
    },
  });

  return (
    <form
      onSubmit={handleSubmit((data) => createSection(data.sectionName))}
      className="create-section-input-container">
      <label>Section name:</label>

      <input
        {...register('sectionName', {
          required: 'section name is required',
          minLength: { value: 2, message: 'min length is 2' },
          maxLength: { value: 20, message: 'max length is 20' },
        })}
        className="create-section-input"
      />

      <Button size="small" type="submit" className="create-section-button">
        Create
      </Button>

      <p style={{ color: 'red' }}>{errors.sectionName?.message}</p>
    </form>
  );
};

export default CreateSectionInput;
