import { Button } from '@mui/material';
import { useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { updateProjectTask } from '../../store/asyncThanks';
import SelectInput from '../board/selectInput';
import { generateIndex } from '../utils/indexExtractor';

import './taskUpdateInput.css';

const TaskUpdateInput = ({ defaultValues, sections, tasks }) => {
  const {
    register,
    handleSubmit,
    watch,
    setValue,
    formState: { errors },
  } = useForm({ defaultValues });

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleUpdate = (data) => {
    const arg = {
      taskId: data._id,
      newTask: {
        title: data.title,
        description: data.description,
        section: data.section,
        index: generateIndex(tasks, data.section),
      },
    };

    dispatch(updateProjectTask(arg));
    navigate(-1);
  };

  return (
    <form onSubmit={handleSubmit(handleUpdate)} className="task-page">
      <div className="input-box task-page_input-box">
        <input
          {...register('title', {
            required: 'Title is required',
            minLength: { value: 2, message: 'min length is 2' },
          })}
          className="input-box_text"
        />
        <p style={{ color: 'red' }}>{errors.title?.message}</p>
      </div>
      <div className="task-page_input-box">
        <textarea
          {...register('description', {
            maxLength: { value: 1000, message: 'max lenght is 1000' },
          })}
          className="task-modal_details"
          rows="15"
          cols="40"
        />
        <p style={{ color: 'red' }}>{errors.description?.message}</p>
      </div>
      <SelectInput
        label="Sections"
        selectedValue={watch('section')}
        data={sections.map(({ sectionName, _id }) => ({ name: sectionName, id: _id }))}
        onSelectHandler={(event) => setValue('section', event.target.value, { shouldDirty: true })}
      />
      <div>
        <Button size="small" type="submit">
          Update
        </Button>
      </div>
    </form>
  );
};

export default TaskUpdateInput;
