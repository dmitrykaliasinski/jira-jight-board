import { Button } from '@mui/material';
import { useState } from 'react';

import './estimateInput.css';

const EstimateInput = ({ onEstimate, estimate, onClose }) => {
  const [estimateValue, setEstimateValue] = useState(estimate);

  const onEstimateSubmit = (e) => {
    e.preventDefault();
    onEstimate(estimateValue);
    onClose(false);
  };

  return (
    <form onSubmit={onEstimateSubmit} className="estimate-form">
      <label htmlFor="estimate">Estimate: </label>
      <input
        value={estimateValue}
        onChange={(e) => setEstimateValue(e.target.value)}
        type="number"
        name="estimate"
        id="estimate"
        placeholder="hours"
        required
      />
      <Button size="small" type="submit">
        submit
      </Button>
    </form>
  );
};

export default EstimateInput;
