import LinearProgress from '@mui/material/LinearProgress';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';

export default function LinearProgressWithLabel({ value, time, color, back }) {
  return (
    <Box sx={{ display: 'flex', alignItems: 'center' }}>
      <Box sx={{ width: '100%', mr: 1, color: '#F2F3F5' }}>
        <LinearProgress
          variant="determinate"
          value={value}
          sx={{ backgroundColor: back, height: '7px', borderRadius: '10px' }}
          color={color}
        />
      </Box>
      <Box sx={{ minWidth: 35 }}>
        <Typography variant="body2" color="text.secondary">{`${time || 0}h`}</Typography>
      </Box>
    </Box>
  );
}
