import { DataGrid } from '@mui/x-data-grid';
import { timeConvert } from '../utils/timeConvertor';

import './tableGrid.css';

const cellClassNames = {
  taskName: 'table-name',
  loggedTime: 'table-logged',
  data: 'cell-data',
};

export default function Table({ tasks, logs }) {
  const columnsFromLogs = logs.map((el, index) => {
    return {
      field: String(index),
      headerName: el.date,
      headerClassName: 'table-header',
      headerAlign: 'center',
      align: 'center',
      sortable: false,
      width: 120,
      renderCell: (params) => {
        if (params.row[params.field]) {
          return timeConvert(params.row[params.field]);
        }
      },
    };
  });

  const columns = [
    {
      field: 'name',
      headerName: 'Task',
      headerClassName: 'table-header',
      headerAlign: 'center',
    },
    {
      field: 'logged',
      headerName: 'Logged Time',
      headerClassName: 'table-header',
      headerAlign: 'center',
    },
    ...columnsFromLogs,
  ];

  const rows = tasks.map((task) => {
    const column = columnsFromLogs.map((column) => {
      const field = logs[column.field].logs
        .filter((log) => log.taskId === task._id)
        .map((log) => log.time);

      if (field.length) {
        return field.reduce((a, b) => +a + +b);
      }

      return null;
    });

    const loggedTime = logs
      .map((el) => el.logs)
      .flat()
      .filter((log) => log.taskId === task._id)
      .map((log) => log.time)
      .reduce((a, b) => +a + +b);

    return Object.assign(
      { id: task._id, name: task.title, logged: timeConvert(loggedTime) },
      column
    );
  });

  return (
    <div className="table-container">
      <DataGrid
        rows={rows}
        columns={columns}
        getCellClassName={(params) => {
          if (params.field === 'name') {
            return cellClassNames.taskName;
          } else if (params.field === 'logged') {
            return cellClassNames.loggedTime;
          } else if (params.row[params.field]) {
            return cellClassNames.data;
          }
        }}
      />
    </div>
  );
}
