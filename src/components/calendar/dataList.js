import LogInfo from '../task/logInfo';

import './dataList.css';

export default function DataTable({ data }) {
  const { date, logs } = data;

  return (
    <div className="dataList-container">
      <h5 className="dataList-title">{date}</h5>
      {logs.map((log) => (
        <LogInfo log={log} key={log.date} classLog="logInfo-table" />
      ))}
    </div>
  );
}
