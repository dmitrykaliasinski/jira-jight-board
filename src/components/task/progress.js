import { useState } from 'react';
import { Button } from '@mui/material';
import EstimateInput from './estimateInput';
import LinearProgressWithLabel from './progressBar';

import './progress.css';

const calculateProgressValue = (logged, estimate, progressType) => {
  if (logged) {
    return logged > estimate ? 100 : Math.round((logged / estimate) * 100);
  }

  return progressType === 'remaining' ? 100 : 0;
};

const Progress = ({ estimate, loggedTime, onEstimate }) => {
  const [isEstimateOpen, setIsEstimateOpen] = useState(false);

  const openEstimateInput = () => {
    setIsEstimateOpen(true);
  };

  let logged = 0;

  if (loggedTime) {
    logged = (loggedTime / 60).toFixed(1);
  }

  const remainingTime = estimate - logged;

  return (
    <div className="estimate_container">
      <div>
        <span>Estimated:</span>
        <LinearProgressWithLabel
          value={estimate ? 100 : 0}
          time={estimate}
          color="primary"
          back="#F2F3F5"
        />
        <span>Remaining:</span>
        <LinearProgressWithLabel
          value={calculateProgressValue(logged, estimate, 'remaining')}
          time={remainingTime > 0 ? remainingTime : 0}
          color="inherit"
          back="orange"
        />
        <span>Logged:</span>
        <LinearProgressWithLabel
          value={calculateProgressValue(logged, estimate, 'logged')}
          time={logged}
          color={logged > estimate ? 'error' : 'success'}
          back="#F2F3F5"
        />
      </div>
      {!isEstimateOpen && (
        <Button size="small" onClick={openEstimateInput}>
          Change Estimate
        </Button>
      )}
      {isEstimateOpen && (
        <EstimateInput onEstimate={onEstimate} estimate={estimate} onClose={setIsEstimateOpen} />
      )}
    </div>
  );
};

export default Progress;
