import { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Button } from '@mui/material';
import { updateProjectTask, createNewTask } from '../../store/asyncThanks';

import './taskModal.css';
import SelectInput from './selectInput';

const TaskModal = ({ modalData, closeModal, createIndex }) => {
  const { sectionName, task } = modalData;
  const [sectionSelect, setSectionSelect] = useState(task ? task.section : '');

  const [title, setTitle] = useState(task ? task.title : 'Title');
  const [description, setDescription] = useState(task ? task.description : 'Description');

  const projectId = useSelector(({ tasks }) => tasks._id);
  const sections = useSelector(({ tasks }) =>
    tasks.sections.map(({ sectionName, _id }) => {
      return {
        name: sectionName,
        id: _id,
      };
    })
  );

  const dispatch = useDispatch();

  const onTitleChange = (e) => setTitle(e.target.value);
  const onDescriptionChange = (e) => setDescription(e.target.value);

  const handleUpdate = () => {
    const arg = {
      taskId: task._id,
      newTask: {
        title: title,
        description: description,
        section: sectionSelect === '' ? task.section : sectionSelect,
        index: createIndex(sectionSelect),
      },
    };

    dispatch(updateProjectTask(arg));
    closeModal();
  };

  const handleCreate = () => {
    const newTask = {
      project: projectId,
      title: title,
      description: description,
      section: sectionName,
      index: createIndex(sectionName),
    };

    dispatch(createNewTask(newTask));
    closeModal();
  };

  const onSelectSectionHandler = (e) => {
    setSectionSelect(e.target.value);
  };

  return (
    <div className="task-modal">
      <div className="input-box">
        <input
          value={title}
          onChange={onTitleChange}
          className="input-box_text"
          type="text"
          name="name"
          id="name"
        />
      </div>
      <div>
        <textarea
          value={description}
          onChange={onDescriptionChange}
          className="task-modal_details"
          name="details"
          id="details"
          rows="5"
          cols="40"
        />
      </div>
      {task && (
        <SelectInput
          label="Sections"
          selectedValue={sectionSelect}
          data={sections}
          onSelectHandler={onSelectSectionHandler}
        />
      )}
      <div>
        <Button size="small" onClick={task ? handleUpdate : handleCreate}>
          {task ? 'Update' : 'Create'}
        </Button>
        <Button size="small" onClick={() => closeModal()} className="cancel-button">
          Cancel
        </Button>
      </div>
    </div>
  );
};

export default TaskModal;
