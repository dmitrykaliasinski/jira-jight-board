const LOGS_URL = 'http://localhost:3001/logs';

export const createLog = async (newLog) => {
  const response = await fetch(LOGS_URL, {
    method: 'POST',
    body: JSON.stringify(newLog),
    headers: {
      'Content-Type': 'application/json',
    },
  });

  return await response.json();
};

export const getLogs = async (taskId, page) => {
  const response = await fetch(`${LOGS_URL}?taskId=${taskId}&page=${page}`);

  return await response.json();
};

export const getAllLogs = async (taskId) => {
  const response = await fetch(`${LOGS_URL}/time?taskId=${taskId}`);

  return await response.json();
};

export const getLogsTime = async (taskId) => {
  const response = await fetch(`${LOGS_URL}/time?taskId=${taskId}`);
  const logs = await response.json();

  if (logs.length) {
    const allTime = logs.map(({ time }) => time).reduce((first, next) => +first + +next);
    return allTime;
  }

  return 0;
};
