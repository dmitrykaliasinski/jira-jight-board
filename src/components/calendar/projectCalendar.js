import { Button } from '@mui/material';
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { getAllLogs } from '../../api/log';
import Table from './tableGrid';

const ProjectCalendar = () => {
  const [logs, setLogs] = useState();
  const navigate = useNavigate();
  const tasks = useSelector((state) => state.tasks.tasks);

  useEffect(() => {
    if (tasks) {
      const fetchAllLogs = async () => {
        const logs = await Promise.all(tasks.map(({ _id }) => getAllLogs(_id)));
        setLogs(logs);
      };

      fetchAllLogs();
    }
  }, [tasks]);

  if (!tasks || !logs) {
    return <p>No tasks or logs!</p>;
  }

  const uniqLogDates = logs
    .flat()
    .sort((a, b) => new Date(a.date) - new Date(b.date))
    .map((log) => log.date.split('T')[0])
    .filter((el, index, logDates) => logDates.indexOf(el) === index);

  const dateWithLogs = uniqLogDates.map((el) => {
    return {
      date: el,
      logs: logs.flat().filter(({ date }) => date.split('T')[0] === el),
    };
  });

  return (
    <div>
      <Button onClick={() => navigate(-1)}>Go Back</Button>
      <Table tasks={tasks} logs={dateWithLogs} />
    </div>
  );
};

export default ProjectCalendar;
