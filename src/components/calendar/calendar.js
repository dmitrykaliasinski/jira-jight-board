import { Button } from '@mui/material';
import { useEffect, useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { getAllLogs } from '../../api/log';
import DataTable from './dataList';

import './calendar.css';
import { timeConvert } from '../utils/timeConvertor';

const TaskLogCalendar = () => {
  const navigate = useNavigate();
  const { taskId } = useParams();

  const [logs, setLogs] = useState();

  useEffect(() => {
    if (taskId) {
      const fetchLogs = async () => {
        const logs = await getAllLogs(taskId);
        setLogs(logs);
      };

      fetchLogs();
    }
  }, [taskId]);

  if (!logs) {
    return <p>No logs are created!</p>;
  }

  const uniqDate = logs
    .map(({ date }) => date.split('T')[0])
    .filter((el, index, logDates) => logDates.indexOf(el) === index);

  const dateWithLogs = uniqDate.map((el) => {
    return {
      date: el,
      logs: logs.filter(({ date }) => date.split('T')[0] === el),
    };
  });

  const allTime = logs.map(({ time }) => time).reduce((first, next) => +first + +next);

  return (
    <div className="calendar-container_outer">
      <Button onClick={() => navigate(-1)}>Go Back</Button>

      <div className="calendar-container">
        <div className="calendar-header">
          <h3 className="calendar-header_name">
            {dateWithLogs[0].date} - {dateWithLogs.at(-1).date}
          </h3>
        </div>

        <div className="calendar-table">
          {dateWithLogs.map((el) => (
            <DataTable data={el} key={el.date} />
          ))}
        </div>
      </div>
      <p className="calendar-logged-time">Logged time: {timeConvert(allTime)}</p>
    </div>
  );
};

export default TaskLogCalendar;
