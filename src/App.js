import { Navigate, Route, Routes } from 'react-router-dom';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';

import Header from './components/header/header';
import SectionGrid from './components/board/sectionsGrid';

import './app.css';
import TaskPage from './components/task/taskPage';
import TaskLogCalendar from './components/calendar/calendar';
import ProjectCalendar from './components/calendar/projectCalendar';

function App() {
  return (
    <div className="container">
      <Header />
      <DndProvider backend={HTML5Backend}>
        <Routes>
          <Route path="/" element={<SectionGrid />} />
          <Route path="*" element={<Navigate to="/" />} />
          <Route path="/calendar/" element={<ProjectCalendar />} />
          <Route path="/task/calendar/:taskId" element={<TaskLogCalendar />} />
          <Route path="/task/:taskId" element={<TaskPage />} />
        </Routes>
      </DndProvider>
    </div>
  );
}

export default App;
