import { Button } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import { pageLimit } from '../utils/pageLimit';
import LogInfo from './logInfo';

import './logList.css';

const maxElPerPage = 5;

const LogList = ({ logData, page, allLogs, changePage, taskId }) => {
  const navigate = useNavigate();

  const increasePage = () => {
    changePage((prev) => prev + 1);
  };

  const decreasePage = () => {
    changePage((prev) => prev - 1);
  };

  return (
    <div className="log-list">
      <h4 className="log-list_header">Log Information</h4>
      {logData.map((log) => (
        <LogInfo log={log} key={log.date} classLog="logInfo-container" />
      ))}
      {allLogs > maxElPerPage && (
        <div className="log-list_pagination">
          <button
            className="log-list_pagination-elements log-list_pagination-button"
            disabled={page > 1 ? false : true}
            onClick={decreasePage}>
            {'<'}
          </button>
          <span className="log-list_pagination-elements">{page}</span>
          <button
            className="log-list_pagination-elements log-list_pagination-button"
            disabled={pageLimit(page, maxElPerPage, allLogs)}
            onClick={increasePage}>
            {'>'}
          </button>
        </div>
      )}

      <Button size="small" onClick={() => navigate('/task/calendar/' + taskId)}>
        View in calendar
      </Button>
    </div>
  );
};

export default LogList;
