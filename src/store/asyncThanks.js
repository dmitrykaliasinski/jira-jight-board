import { createAsyncThunk } from '@reduxjs/toolkit';
import {
  getProject,
  createProject,
  createSection,
  deleteProject,
  deleteSection,
} from '../api/project';
import { createTask, deleteTask, updateTask } from '../api/task';

export const loadProject = createAsyncThunk(
  'tasks/loadProject',
  async (projectId) => await getProject(projectId)
);

export const createNewTask = createAsyncThunk(
  'tasks/createNewTask',
  async (newTask) => await createTask(newTask)
);

export const deleteProjectTask = createAsyncThunk(
  'tasks/deleteProjectTask',
  async (taskId) => await deleteTask(taskId)
);

export const updateProjectTask = createAsyncThunk(
  'tasks/updateProjectTask',
  async (arg) => await updateTask(arg.taskId, arg.newTask)
);

export const createNewProject = createAsyncThunk(
  'tasks/createNewProject',
  async (projectName) => await createProject(projectName)
);
export const deleteProjectAsync = createAsyncThunk('tasks/deleteProject', async (projectId) => {
  await deleteProject(projectId);
  return {};
});

export const createProjectSection = createAsyncThunk(
  'tasks/createProjectSection',
  async (arg) => await createSection(arg.projectId, arg.sectionName)
);

export const deleteProjectSection = createAsyncThunk(
  'tasks/deleteProjectSection',
  async (arg) => await deleteSection(arg.projectId, arg.sectionName)
);
