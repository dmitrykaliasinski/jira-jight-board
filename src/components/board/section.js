import { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { useDrag, useDrop } from 'react-dnd';
import update from 'immutability-helper';

import { deleteProjectTask } from '../../store/asyncThanks';
import { updateProjectTask } from '../../store/asyncThanks';
import TaskCard from './taskCard';
import { ItemTypes } from '../../constants/dragTypes';
import { moveTaskCard } from '../../store/tasksSlice';
import { updateProject } from '../../api/project';
import { updateTask } from '../../api/task';
import { generateIndex } from '../utils/indexExtractor';

import './section.css';

const Section = ({
  sectionName,
  data,
  openModal,
  deleteSection,
  editMode,
  moveSection,
  project,
}) => {
  const [, drop] = useDrop(
    () => ({
      accept: ItemTypes.Cards,
      drop: (item) => {
        if (sectionName === item.section) {
          return Promise.all(data.map((task) => updateTask(task._id, task)));
        }

        const arg = {
          taskId: item._id,
          newTask: {
            project: project._id,
            title: item.title,
            description: item.description,
            section: sectionName,
            index: generateIndex(data, sectionName),
          },
        };

        dispatch(updateProjectTask(arg));
      },
    }),
    [data]
  );

  const [{ isDragging }, drag] = useDrag(
    () => ({
      type: ItemTypes.Sections,
      item: { sectionName },
      collect: (monitor) => ({
        isDragging: !!monitor.isDragging(),
      }),
      end: () => updateProject(project._id, project),
    }),
    [sectionName, project]
  );

  const [, dropSec] = useDrop(
    () => ({
      accept: ItemTypes.Sections,
      hover: ({ sectionName: draggedSection }) => {
        if (draggedSection !== sectionName) {
          moveSection(draggedSection, sectionName);
        }
      },
    }),
    [moveSection, sectionName]
  );

  const dispatch = useDispatch();

  const deleteCardHandler = (taskId) => {
    dispatch(deleteProjectTask(taskId));
  };

  const createTaskModalHandler = () => {
    openModal(sectionName);
  };

  const moveCard = useCallback(
    (draggedId, overId) => {
      const task = project.tasks.find((task) => task._id === draggedId);
      const fromIndex = project.tasks.indexOf(task);
      const overIndex = project.tasks.findIndex((task) => task._id === overId);

      const newTasks = update(project.tasks, {
        $splice: [
          [fromIndex, 1],
          [overIndex, 0, task],
        ],
      });

      const orderedTasks = newTasks.map((task, idx) => ({ ...task, index: idx }));

      dispatch(moveTaskCard(orderedTasks));
    },
    [project, dispatch]
  );

  const deleteSectionHandler = () => {
    deleteSection(sectionName);
  };

  return (
    <div
      className="section-greed"
      ref={(node) => editMode && drag(dropSec(node))}
      style={{
        opacity: isDragging ? 0 : 1,
        cursor: editMode && 'grab',
        boxShadow: editMode && '0 0 10px rgba(0,0,0,0.3)',
      }}>
      <h5 className="section-name">{sectionName}</h5>
      <p className="section-greed_create-task" onClick={createTaskModalHandler}>
        +
      </p>
      <ul ref={drop} className="section-card-list">
        {data
          .sort((a, b) => a.index - b.index)
          .map((task) => (
            <li key={task._id} className="section-list">
              <TaskCard
                task={task}
                onDelete={deleteCardHandler}
                openModal={openModal}
                editMode={editMode}
                moveCard={moveCard}></TaskCard>
            </li>
          ))}
      </ul>
      <h5 className="delete-section" onClick={deleteSectionHandler}>
        Delete Section
      </h5>
    </div>
  );
};

export default Section;
