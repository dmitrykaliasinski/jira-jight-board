import { Button } from '@mui/material';
import { useForm } from 'react-hook-form';
import DayPicker from './dayPicker';

import './logWorkModal.css';

const timeStringToMinutes = (time) => {
  const splittedTime = time.match(/[a-zA-Z]+|[0-9]+/g);

  if (splittedTime[2]) {
    return +splittedTime[0] * 60 + +splittedTime[2];
  }
  if (splittedTime[1] && splittedTime[1].startsWith('m')) {
    return +splittedTime[0];
  }

  return +splittedTime[0] * 60;
};

const validateTime = (value, values) => {
  const arrString = value.split('');
  const mins = arrString.filter((letter) => letter === 'm');
  const hours = arrString.filter((letter) => letter === 'h');
  const regex = value.match(/[a-zA-Z]+|[0-9]+/g);

  if (regex[1] && !(regex[1].startsWith('h') || regex[1].startsWith('m'))) {
    return '1h, 30m, 1h 30m...';
  }
  if (regex[2] && isNaN(regex[2])) {
    return '1h, 30m, 1h 30m...';
  }
  if (regex[3] && !regex[3].startsWith('m')) {
    return '1h, 30m, 1h 30m...';
  }
  if (mins.length > 1 || hours.length > 1) {
    return 'should have only one hour/minute select';
  }
  if (
    arrString.indexOf('m') > 0 &&
    arrString.indexOf('h') > 0 &&
    arrString.indexOf('h') > arrString.indexOf('m')
  ) {
    return 'first write hours then minutes';
  }
};

const LogWorkModal = ({ closeModal, onSubmitForm }) => {
  const {
    register,
    setValue,
    watch,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {
      description: '',
      time: '',
      date: new Date(),
    },
  });

  const onLog = (data) => {
    const { description, time, date } = data;
    onSubmitForm({
      description,
      date,
      time: timeStringToMinutes(time),
    });
    closeModal();
  };

  return (
    <form className="log-modal" onSubmit={handleSubmit(onLog)}>
      <h2>Log Work</h2>
      <div>
        <textarea
          {...register('description', {
            required: 'description is required',
            minLength: {
              value: 4,
              message: 'required at least 4 letters',
            },
            maxLength: {
              value: 400,
              message: 'not more then 400 letters',
            },
          })}
          placeholder="description"
          className="log-modal_details"
          rows="5"
          cols="40"
        />
        <p style={{ color: 'red' }}>{errors.description?.message}</p>
      </div>

      <input
        {...register('time', {
          required: 'time is required',
          pattern: /[h,' ',m]+|[0-9]+/,
          validate: {
            firstIsNumber: (v) => (isNaN(v[0]) ? '1h, 30m, 1h 30m...' : undefined),
            noHoursAfterMinutes: (value, values) => validateTime(value, values),
          },
        })}
        placeholder="1h 30m"
        className="log-modal_time"
      />
      <p style={{ color: 'red', marginRight: 'auto', marginTop: '-20px' }}>
        {errors.time?.message}
      </p>

      <DayPicker onSelect={setValue} getValue={watch} />
      <div className="log-modal_buttons">
        <Button type="submit">Save</Button>
        <Button onClick={() => closeModal()}>Cancel</Button>
      </div>
    </form>
  );
};

export default LogWorkModal;
