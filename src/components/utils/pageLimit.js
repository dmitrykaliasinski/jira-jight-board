export const pageLimit = (pageNumber, countOnPage, allElements) =>
  allElements - (pageNumber - 1) * countOnPage <= countOnPage;
