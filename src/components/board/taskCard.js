import { Button } from '@mui/material';
import { useDrag, useDrop } from 'react-dnd';
import { useNavigate } from 'react-router-dom';
import { ItemTypes } from '../../constants/dragTypes';

import './taskCard.css';

const TaskCard = ({ task, onDelete, openModal, moveCard, editMode }) => {
  const navigate = useNavigate();

  const [{ isDragging }, drag] = useDrag(
    () => ({
      type: ItemTypes.Cards,
      item: { ...task },
      collect: (monitor) => ({
        isDragging: !!monitor.isDragging(),
      }),
    }),
    [task]
  );

  const [, drop] = useDrop(
    () => ({
      accept: ItemTypes.Cards,
      hover: ({ _id: draggedId, section }) => {
        if (draggedId !== task._id && section === task.section) {
          moveCard(draggedId, task._id);
        }
      },
    }),
    [moveCard, task]
  );

  return (
    <div
      className="task-card"
      ref={(node) => !editMode && drag(drop(node))}
      style={{
        opacity: isDragging ? 0 : 1,
        cursor: isDragging && 'wait',
        boxShadow: editMode && 'none',
      }}>
      <h5 className="task-card_name">{task.title}</h5>
      <div>
        <Button size="small" onClick={() => !editMode && navigate('/task/' + task._id)}>
          View
        </Button>
        <Button size="small" onClick={() => !editMode && onDelete(task._id)}>
          Delete
        </Button>
      </div>
    </div>
  );
};

export default TaskCard;
