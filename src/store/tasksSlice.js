import { createSlice } from '@reduxjs/toolkit';
import {
  loadProject,
  createNewTask,
  deleteProjectTask,
  updateProjectTask,
  createNewProject,
  createProjectSection,
  deleteProjectAsync,
  deleteProjectSection,
} from './asyncThanks';

const tasksSlice = createSlice({
  name: 'tasks',
  initialState: {},
  reducers: {
    moveTaskCard: (state, action) => {
      state.tasks = action.payload;
    },
    moveSectionList: (state, action) => {
      state.sections = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(loadProject.fulfilled, (state, action) => {
      localStorage.setItem('lastOpenProjectId', action.payload.projectName);

      return action.payload;
    });
    builder.addCase(createNewTask.fulfilled, (state, action) => {
      state.tasks = [...state.tasks, action.payload];
    });
    builder.addCase(deleteProjectTask.fulfilled, (state, action) => {
      const taskIdx = state.tasks.findIndex(({ _id }) => _id === action.payload._id);
      state.tasks.splice(taskIdx, 1);
    });
    builder.addCase(updateProjectTask.fulfilled, (state, action) => {
      const taskIdx = state.tasks.findIndex(({ _id }) => _id === action.payload._id);
      state.tasks.splice(taskIdx, 1, action.payload);
    });
    builder.addCase(createNewProject.fulfilled, (state, action) => {
      localStorage.setItem('lastOpenProjectId', action.payload.projectName);
      return action.payload;
    });
    builder.addCase(deleteProjectAsync.fulfilled, (state, action) => {
      return {};
    });
    builder.addCase(createProjectSection.fulfilled, (state, action) => {
      return action.payload;
    });
    builder.addCase(deleteProjectSection.fulfilled, (state, action) => {
      return action.payload;
    });
  },
});

export const { moveTaskCard, moveSectionList } = tasksSlice.actions;
export default tasksSlice.reducer;
