const TASKS_URL = 'http://localhost:3001/tasks';

export const getTask = async (taskId) => {
  const response = await fetch(`${TASKS_URL}/${taskId}`);
  const task = await response.json();

  return task;
};

export const createTask = async (newTask) => {
  const response = await fetch(TASKS_URL, {
    method: 'POST',
    body: JSON.stringify(newTask),
    headers: {
      'Content-Type': 'application/json',
    },
  });

  return await response.json();
};

export const deleteTask = async (taskId) => {
  const response = await fetch(`${TASKS_URL}/${taskId}`, {
    method: 'DELETE',
  });

  return await response.json();
};

export const updateTask = async (taskId, newTask) => {
  const response = await fetch(`${TASKS_URL}/${taskId}`, {
    method: 'PUT',
    body: JSON.stringify(newTask),
    headers: {
      'Content-Type': 'application/json',
    },
  });

  return await response.json();
};
