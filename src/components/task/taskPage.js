import { Button } from '@mui/material';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import { Modal } from '@mui/material';

import { updateProjectTask } from '../../store/asyncThanks';
import LogWorkModal from './logWorkModal';
import LogList from './logList';
import { createLog, getLogs, getLogsTime } from '../../api/log';
import Progress from './progress';

import './taskPage.css';
import TaskUpdateInput from './taskUpdateInput';

const TaskPage = () => {
  const [task, setTask] = useState();
  const [logPage, setLogPage] = useState(1);
  const [logData, setLogData] = useState();
  const [open, setOpen] = useState(false);

  const dispatch = useDispatch();
  const { taskId } = useParams();
  const navigate = useNavigate();

  const data = useSelector(({ tasks }) => tasks);

  useEffect(() => {
    if (data.tasks) {
      const task = data.tasks.find((task) => task._id === taskId);
      setTask(task);
    }
  }, [data, taskId]);

  useEffect(() => {
    const fetchLogs = async () => {
      const logs = await getLogs(taskId, logPage);

      const allTime = await getLogsTime(taskId);

      setLogData({ ...logs, allTime });
    };

    fetchLogs();
  }, [logPage, taskId]);

  const closeModalHandler = () => setOpen(false);

  const saveLog = async (data) => {
    try {
      const newLog = await createLog({ ...data, taskId });

      if (logData) {
        setLogData(({ logs, logCount, allTime }) => {
          const count = logCount + 1;
          const newLogs = [newLog, ...logs.splice(0, 4)];
          return {
            logs: newLogs,
            logCount: count,
            allTime: allTime + +newLog.time,
          };
        });
      } else {
        setLogData({
          logs: [newLog],
          logCount: 1,
          allTime: newLog.time,
        });
      }
    } catch (err) {
      throw new Error(err.message);
    }
  };

  const updateEstimate = (value) => {
    const arg = {
      taskId: task._id,
      newTask: {
        estimate: value,
      },
    };

    dispatch(updateProjectTask(arg));
  };

  if (!task) {
    return <p>Searching for task...</p>;
  }

  return (
    <>
      <div className="task-container">
        <TaskUpdateInput defaultValues={task} sections={data.sections} tasks={data.tasks} />

        <div className="progress-block">
          <Progress
            estimate={+task.estimate}
            loggedTime={logData ? logData.allTime : 0}
            onEstimate={updateEstimate}
          />
          <Button size="small" onClick={() => setOpen(true)} className="cancel-button">
            Log Work
          </Button>
        </div>

        <div>
          {logData && logData.logs.length > 0 && (
            <LogList
              logData={logData.logs}
              page={logPage}
              allLogs={logData.logCount}
              changePage={setLogPage}
              taskId={taskId}
            />
          )}
        </div>
      </div>
      <div className="task-page_buttons-container">
        <Button size="small" onClick={() => navigate(-1)} className="cancel-button">
          go back
        </Button>
      </div>
      <Modal open={open} onClose={closeModalHandler}>
        <div className="task-modal">
          <LogWorkModal closeModal={closeModalHandler} onSubmitForm={saveLog} />
        </div>
      </Modal>
    </>
  );
};

export default TaskPage;
