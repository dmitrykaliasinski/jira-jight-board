export const timeConvert = (min) => {
  const hours = min / 60;
  const roundedHours = Math.floor(hours);
  const minutes = Math.round((hours - roundedHours) * 60);

  if (roundedHours) {
    return minutes ? `${roundedHours}h ${minutes}m` : `${roundedHours}h`;
  }

  return `${minutes}m`;
};
