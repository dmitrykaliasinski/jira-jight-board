import { useForm } from 'react-hook-form';
import { Button } from '@mui/material';

import './createProjectModal.css';

const CreateProjectModal = ({ closeModal, createProject }) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {
      projectName: '',
    },
  });

  return (
    <form onSubmit={handleSubmit((data) => createProject(data.projectName))}>
      <div className="create-project_input-container">
        <label>Project name:</label>
        <input
          {...register('projectName', {
            required: 'project name is required',
            minLength: {
              value: 2,
              message: 'min length is 2',
            },
            maxLength: {
              value: 20,
              message: 'max length is 20',
            },
          })}
          placeholder="project name"
          className="create-project_input"
        />
        <p style={{ color: 'red' }}>{errors.projectName?.message}</p>
      </div>
      <div className="create-project_buttons">
        <Button size="small" className="create-project_button" type="submit">
          Create
        </Button>
        <Button size="small" className="create-project_button" onClick={() => closeModal()}>
          Close
        </Button>
      </div>
    </form>
  );
};

export default CreateProjectModal;
