const PROJECTS_URL = 'http://localhost:3001/projects';

export const getProjectsNamesAndIds = async () => {
  const response = await fetch(PROJECTS_URL);
  const projects = await response.json();

  const namesAndIds = projects.map(({ projectName, _id }) => {
    return {
      name: projectName,
      id: _id,
    };
  });

  return namesAndIds;
};

export const getProject = async (id) => {
  const response = await fetch(`${PROJECTS_URL}/${id}`);

  return await response.json();
};

export const createProject = async (project) => {
  const response = await fetch(PROJECTS_URL, {
    method: 'POST',
    body: JSON.stringify(project),
    headers: {
      'Content-Type': 'application/json',
    },
  });

  return await response.json();
};

export const updateProject = async (id, newData) => {
  const response = await fetch(`${PROJECTS_URL}/${id}`, {
    method: 'PUT',
    body: JSON.stringify(newData),
    headers: {
      'Content-Type': 'application/json',
    },
  });

  return await response.json();
};

export const deleteProject = async (id) =>
  await fetch(`${PROJECTS_URL}/${id}`, {
    method: 'DELETE',
  });

export const createSection = async (projectId, sectionName) => {
  const response = await fetch(`${PROJECTS_URL}/${projectId}`);
  const project = await response.json();

  const section = {
    sectionName: sectionName,
  };

  project.sections.push(section);

  return await updateProject(projectId, project);
};

export const deleteSection = async (projectId, sectionName) => {
  const response = await fetch(`${PROJECTS_URL}/${projectId}`);
  const project = await response.json();

  const sectionIndex = project.sections.findIndex((section) => section.sectionName === sectionName);

  project.sections.splice(sectionIndex, 1);

  return await updateProject(projectId, project);
};
