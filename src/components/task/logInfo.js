import { timeConvert } from '../utils/timeConvertor';
import './logInfo.css';

const LogInfo = ({ log, classLog }) => {
  if (!log) {
    return <div className={classLog}></div>;
  }

  return (
    <div className={classLog}>
      <p>
        {log.date.split('T')[0]} - {timeConvert(log.time)}
      </p>
      <p>{log.description}</p>
    </div>
  );
};

export default LogInfo;
