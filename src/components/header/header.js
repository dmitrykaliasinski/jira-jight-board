import { Button, Modal } from '@mui/material';
import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { NavLink, useNavigate } from 'react-router-dom';
import { getProjectsNamesAndIds } from '../../api/project';
import { createNewProject, deleteProjectAsync, loadProject } from '../../store/asyncThanks';
import SelectInput from '../board/selectInput';
import CreateProjectModal from './createProjectModal';

import './header.css';

const Header = () => {
  const [createProjectModal, setCreateProjectModal] = useState(false);
  const [project, setProject] = useState('');
  const [projects, setProjects] = useState();

  const navigation = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    const getAllProjects = async () => {
      const allProjects = await getProjectsNamesAndIds();

      setProjects(allProjects);
    };

    getAllProjects();
  }, [project]);

  useEffect(() => {
    const lastOpenProject = localStorage.getItem('lastOpenProjectId');

    if (lastOpenProject && projects) {
      setProject(lastOpenProject);

      const projectId = projects.find((project) => {
        return project.name === lastOpenProject;
      }).id;

      dispatch(loadProject(projectId));
    }
  }, [projects, dispatch]);

  const onSelectProjectHandler = (event) => {
    const projectName = event.target.value;
    setProject(projectName);

    const projectId = projects.find((project) => project.name === projectName).id;

    dispatch(loadProject(projectId));
    navigation('/');
  };

  const closeModalHandler = () => setCreateProjectModal(false);

  const openModalHandler = () => setCreateProjectModal(true);

  const createProject = async (projectName) => {
    const project = {
      projectName,
      sections: [],
    };
    await dispatch(createNewProject(project));

    setProject(projectName);

    closeModalHandler();
    navigation('/');
  };

  const deleteProject = () => {
    const projectId = projects.find(({ name }) => name === project).id;
    dispatch(deleteProjectAsync(projectId));

    localStorage.removeItem('lastOpenProjectId');

    setProject('');
    setProjects((curr) => curr.filter((project) => project.id !== projectId));
    navigation('/');
  };

  return (
    <div className="header-container">
      <h1>
        <NavLink to="/" className="header-name">
          Board
        </NavLink>
      </h1>

      <Button variant="text" size="large" className="header-name" onClick={openModalHandler}>
        Create project
      </Button>
      <div className="header_projects-container">
        {project && (
          <Button size="large" className="header-name" onClick={deleteProject}>
            Delete Project
          </Button>
        )}

        <SelectInput
          label="Projects"
          selectedValue={project}
          data={projects}
          onSelectHandler={onSelectProjectHandler}
        />
      </div>

      <Modal open={createProjectModal} onClose={closeModalHandler}>
        <div className="task-modal">
          <CreateProjectModal closeModal={closeModalHandler} createProject={createProject} />
        </div>
      </Modal>
    </div>
  );
};

export default Header;
