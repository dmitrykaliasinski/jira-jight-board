import { FormControl, InputLabel, MenuItem, Select } from '@mui/material';

const SelectInput = ({ selectedValue, data, onSelectHandler, label }) => {
  return (
    <FormControl>
      <InputLabel>{label}</InputLabel>
      <Select
        value={selectedValue}
        label="Sections"
        labelId="demo-simple-select-label"
        id="demo-simple-select"
        sx={{ width: '120px' }}
        onChange={(e) => onSelectHandler(e)}>
        {data &&
          data.map(({ name, id }) => (
            <MenuItem value={name} key={id}>
              {name}
            </MenuItem>
          ))}
      </Select>
    </FormControl>
  );
};

export default SelectInput;
