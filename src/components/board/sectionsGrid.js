import { useCallback, useState } from 'react';
import { Button } from '@mui/material';
import { useSelector, useDispatch } from 'react-redux';
import { useDrop } from 'react-dnd';
import { Modal } from '@mui/material';

import Section from './section';
import TaskModal from './taskModal';
import CreateSectionInput from './createSectionInput';
import { createProjectSection, deleteProjectSection } from '../../store/asyncThanks';
import { ItemTypes } from '../../constants/dragTypes';
import { moveSectionList } from '../../store/tasksSlice';
import { generateIndex } from '../utils/indexExtractor';

import './sectionsGrid.css';
import { useNavigate } from 'react-router-dom';

const SectionGrid = () => {
  const [editMode, setEditMode] = useState(false);
  const [open, setOpen] = useState(false);
  const [createSectionInput, setCreateSectionInput] = useState(false);
  const [modalData, setModalData] = useState({
    sectionName: '',
    task: {},
  });

  const data = useSelector((state) => state.tasks);

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [, drop] = useDrop(() => ({ accept: ItemTypes.Sections }));

  const openModalHandler = (sectionName, task) => {
    setModalData((current) => {
      return { ...current, sectionName, task };
    });
    setOpen(true);
  };
  const closeModalHandler = () => setOpen(false);

  const createSection = (sectionName) => {
    dispatch(createProjectSection({ projectId: data._id, sectionName }));
    setCreateSectionInput(false);
  };

  const deleteSection = (sectionName) => {
    dispatch(deleteProjectSection({ projectId: data._id, sectionName }));
  };

  const editModeHandler = () => setEditMode(!editMode);

  const moveSection = useCallback(
    (draggedSectionName, endSectionName) => {
      const draggedIndex = data.sections.findIndex(
        (section) => section.sectionName === draggedSectionName
      );
      const draggedSection = data.sections.find(
        (section) => section.sectionName === draggedSectionName
      );
      const endIndex = data.sections.findIndex((section) => section.sectionName === endSectionName);

      const sections = [...data.sections];

      sections.splice(draggedIndex, 1);
      sections.splice(endIndex, 0, draggedSection);

      dispatch(moveSectionList(sections));
    },
    [data, dispatch]
  );

  const createIndex = useCallback(
    (sectionName) => {
      return generateIndex(data.tasks, sectionName);
    },
    [data]
  );

  if (!data.hasOwnProperty('sections')) {
    return (
      <div className="no-project-message">
        <h2>Choose or create project!</h2>
      </div>
    );
  }

  return (
    <div className="section-grid">
      <div className="section-grid_title">
        <h3 className="section-grid_name">{data.projectName}</h3>
        <Button
          className="section-grid_create"
          onClick={() => setCreateSectionInput(!createSectionInput)}>
          {createSectionInput ? 'I have all sections' : 'Create New Section'}
        </Button>
        {createSectionInput && <CreateSectionInput createSection={createSection} />}
        {!createSectionInput && (
          <Button
            className="section-grid_create"
            sx={() => editMode && { color: 'red' }}
            onClick={editModeHandler}>
            Edit
          </Button>
        )}
        {!createSectionInput && <Button onClick={() => navigate('calendar/')}>Calendar</Button>}
      </div>
      <ul className="section-grid_list" ref={drop}>
        {data.sections.map((section) => (
          <li key={section._id} className="section-grid_list-li">
            <Section
              sectionName={section.sectionName}
              data={data.tasks.filter((task) => task.section === section.sectionName)}
              openModal={openModalHandler}
              deleteSection={deleteSection}
              editMode={editMode}
              moveSection={moveSection}
              project={data}
            />
          </li>
        ))}
      </ul>
      <Modal open={open} onClose={closeModalHandler}>
        <div className="task-modal">
          <TaskModal
            modalData={modalData}
            closeModal={closeModalHandler}
            createIndex={createIndex}
          />
        </div>
      </Modal>
    </div>
  );
};

export default SectionGrid;
