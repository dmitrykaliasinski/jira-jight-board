export const generateIndex = (tasks, sectionName) => {
  const sectionTasks = tasks.filter((task) => task.section === sectionName);
  if (sectionTasks.length) {
    return sectionTasks.at(-1).index + 1;
  }

  return 1;
};
